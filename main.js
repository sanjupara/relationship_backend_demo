const express = require('express');
const session = require('express-session');
const cors = require('cors');
const CoupleRouter = require('./routes/CoupleRouter');


const app = express();
const port = 3000;
app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use(session({
    secret: "supersecret",
    resave: false,
    saveUninitialized: true,
    cookie: {}
}));

app.listen(port, () => {
    console.log("Currently running on port " + port);
});

app.use('/couples', CoupleRouter);
