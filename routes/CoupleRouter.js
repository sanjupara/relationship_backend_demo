const Router = require('express').Router;

const CoupleRouter = Router();

let coupleData = {
  "name": "Test",
  "TogetherSince": "2022-12-17",
  "image": "https://cdn.pixabay.com/photo/2017/07/31/21/04/people-2561053_1280.jpg",
};

CoupleRouter.get('/', (request, response) => {
  response.send(coupleData);
});

CoupleRouter.put('/update', (request, response) => {
  const updatedCouple = request.body;
  coupleData = { ...coupleData, ...updatedCouple };
  response.send(coupleData);
});

module.exports = CoupleRouter;
